import React, { Component } from 'react'

export default class SmsMms extends Component {
  render() {
    return (
      <div>
        <div className="main_container">
<div className="page_content">
<section className="upper_section">
  <h4>SMS/MMS</h4>
  
</section>
<style>

</style>
<section className="lower_section">
<div className="report-tab">
<div className="tab-content">
	<div role="tabpanel" className="tab-pane active" id="Contacts">

		<div className="sms-sec col-md-12 no-padding">
			<div className="col-md-3 col-sm-3 no-padding-left">
				<div className="team-side-a ">
                        <h4>Select below number for Read & Write message</h4>
				<a className="btn btn-red" data-toggle="modal" data-target="#send-later" href="#">New Message</a>
				<ul className="team-list">
				<li className="active"><a href="#"><span className="flg"><img src="assets/img/india.png" className="img"/></span>(+91) 9418394421</a></li>
					  <li><a href="#"><span className="flg"><img src="assets/img/india.png" className="img"/></span>(+91) 9418394421</a></li>
                      <li><a href="#"><span className="flg"><img src="assets/img/india.png" className="img"/></span>(+91) 9418394421</a></li>
                      <li><a href="#"><span className="flg"><img src="assets/img/india.png" className="img"/></span>(+91) 9418394421</a></li>
                      <li><a href="#"><span className="flg"><img src="assets/img/india.png" className="img"/></span>(+91) 9418394421</a></li>
				</ul>
				</div>
			 </div>
			<div className="chat-sec-b col-md-9 col-sm-9">
				<div className="sms-chat-window">
					<div className="chatbody">
					<div className="panel panel-primary">
				  <div className="panel-heading top-bar">
					  <div className="col-md-8 col-xs-8">
						  <h3 className="panel-title"><span className="glyphicon glyphicon-comment"></span> (+91) 9418394421)</h3>
					  </div>
				  </div>
				  <div className="panel-body msg_container_base">
					  <div className="row msg_container base_sent">
						  <div className="col-md-8 col-xs-8">
							  <div className="messages msg_sent">
								  <p>hii</p>
								  <time className="time_date"> 11:01 AM    |    June 9</time>
							  </div>
						  </div>
						  <div className="col-md-4 col-xs-4 avatar">
						  </div>
					  </div>
					  <div className="row msg_container base_receive">
						  <div className="col-md-4 col-xs-4 avatar">
						  </div>
						  <div className="col-md-8 col-xs-8">
							  <div className="messages msg_receive">
								  <p>hello</p>
								  <time className="time_date"> 11:01 AM    |    June 9</time>
							  </div>
						  </div>
					  </div>
					  <div className="row msg_container base_sent">
						<div className="col-md-8 col-xs-8">
							<div className="messages msg_sent">
								<p>hii</p>
								<time className="time_date"> 11:01 AM    |    June 9</time>
							</div>
						</div>
						<div className="col-md-4 col-xs-4 avatar">
						</div>
					</div>
					<div className="row msg_container base_receive">
						<div className="col-md-4 col-xs-4 avatar">
						</div>
						<div className="col-md-8 col-xs-8">
							<div className="messages msg_receive">
								<p>hello</p>
								<time className="time_date"> 11:01 AM    |    June 9</time>
							</div>
						</div>
                    </div>
                    <div className="row msg_container base_sent">
						<div className="col-md-8 col-xs-8">
							<div className="messages msg_sent">
								<p>hii</p>
								<time className="time_date"> 11:01 AM    |    June 9</time>
							</div>
						</div>
						<div className="col-md-4 col-xs-4 avatar">
						</div>
					</div>
					<div className="row msg_container base_receive">
						<div className="col-md-4 col-xs-4 avatar">
						</div>
						<div className="col-md-8 col-xs-8">
							<div className="messages msg_receive">
								<p>hello</p>
								<time className="time_date"> 11:01 AM    |    June 9</time>
							</div>
						</div>
                    </div>
                    <div className="row msg_container base_sent">
						<div className="col-md-8 col-xs-8">
							<div className="messages msg_sent">
								<p>hii</p>
								<time className="time_date"> 11:01 AM    |    June 9</time>
							</div>
						</div>
						<div className="col-md-4 col-xs-4 avatar">
						</div>
					</div>
					<div className="row msg_container base_receive">
						<div className="col-md-4 col-xs-4 avatar">
						</div>
						<div className="col-md-8 col-xs-8">
							<div className="messages msg_receive">
								<p>hello</p>
								<time className="time_date"> 11:01 AM    |    June 9</time>
							</div>
						</div>
					</div>
				  </div>
				  <div className="panel-footer">
					  <div className="input-group">
						  <input id="btn-input" type="text" className="form-control input-sm chat_input" placeholder="Write your message here..."/>
						  &nbsp;
						  <span className="input-group-btn">
						   <button className="btn btn-blue btn-sm">Send</button>
						    </span>
					  </div>
				  </div>
			  </div>
  
				   </div>
			   </div>
				 
				  </div>
			</div>
			</div>
      </div>
    
<div className="modal send-modal fade" id="send-later" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div className="modal-dialog modal-md" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="exampleModalLabel"> Compose Message
            <button type="button" className="close pull-right" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          </h5>
        </div>
        <div className="modal-body">
            <div className="form-group">
              
                 </div>
             <div className="form-group">
                    <label>To</label>
             <input type="type" className="form-control" id="name" aria-describedby="emailHelp"/>
                 </div>
                     
                            <div className="form-group">
                                <label>Message</label>
                                <textarea id="msg1"  className="form-control chat-md-textarea"></textarea>
                                </div>
           </div>
        <div className="modal-footer ">
            <div className="col-md-12 text-center">
          <button type="button" className="btn btn-red">Send</button>
                     </div> 
        </div> 
      </div>
    </div>
  </div>

          </div>
          </section>
        </div>
     
    </div>
      </div>
    )
  }
}
