import React, { Component } from 'react'

export default class Contacts extends Component {
    render() {
        return (
            <div>
                <div className="main_container">
                    <div className="page_content">
                        <section className="upper_section">
                            <h4>Contacts</h4>
                            <div className="pull-right">
                                <a className="btn btn-red  " href="#" >Import Contact </a>&nbsp;&nbsp;
         		 <a className="btn btn-red " href="#" data-toggle="modal" data-target="#upload-voice">Add Contact </a>
                            </div>
                        </section>
                        <section className="lower_section">
                            <div className="report-tab">
                                <div className="tab-content">


                                    <div className="modal user-ext fade" id="upload-voice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div className="modal-dialog modal-md" role="document">
                                            <div className="modal-content">
                                                <div className="modal-header">
                                                    <h5 className="modal-title" id="exampleModalLabel"> Add User
          <button type="button" className="close pull-right" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                    </h5>
                                                </div>
                                                <div className="modal-body text-center">
                                                    <div className="form-group">

                                                    </div>
                                                    <div className="form-group">
                                                        <input type="type" className="form-control" id="name" aria-describedby="emailHelp" placeholder="First Name" />
                                                    </div>
                                                    <div className="form-group">
                                                        <input type="type" className="form-control" id="email" aria-describedby="emailHelp" placeholder="Last Name" />
                                                    </div>
                                                    <div className="form-group">
                                                        <input type="type" className="form-control" id="phone" aria-describedby="emailHelp" placeholder="Email Address" />
                                                    </div>
                                                    <div className="form-group">
                                                        <input type="type" className="form-control" id="extension" aria-describedby="emailHelp" placeholder="Phone" />
                                                    </div>
                                                </div>
                                                <div className="modal-footer ">
                                                    <div className="col-md-12 text-center">

                                                        <button type="button" className="btn btn-red">Create</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div className="user-ext custom_table col-md-12">
                                        <div className="row margin-btm-30 col-md-width">
                                            <div className="col-md-3 col-sm-4 text-center">
                                                <div className="user-ext-box contact-box">

                                                    <div className="user-ext-details">
                                                        <h2>Jhone Smith</h2>
                                                        <p>demo@gmail.com</p>
                                                        <p>(+91)9418394421</p>
                                                        <div className="row icon-div">
                                                            <a className="call-icon" href="#"><i className="icon-phone icons icons"></i></a>
                                                            <a href="#"><i className="glyphicon glyphicon-edit"></i></a><a className="del" href="#"><i className="glyphicon glyphicon-trash"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-3 col-sm-4 text-center">
                                                <div className="user-ext-box contact-box">
                                                    <div className="user-ext-details">
                                                        <h2>Jhone Smith</h2>
                                                        <p>demo@gmail.com</p>
                                                        <p>(+91)9418394421</p>
                                                        <div className="row icon-div">
                                                            <a className="call-icon" href="#"><i className="icon-phone icons icons"></i></a>
                                                            <a href="#"><i className="glyphicon glyphicon-edit"></i></a><a className="del" href="#"><i className="glyphicon glyphicon-trash"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-3 col-sm-4 text-center">
                                                <div className="user-ext-box contact-box">
                                                    <div className="user-ext-details">
                                                        <h2>Jhone Smith</h2>
                                                        <p>demo@gmail.com</p>
                                                        <p>(+91)9418394421</p>
                                                        <div className="row icon-div">
                                                            <a className="call-icon" href="#"><i className="icon-phone icons icons"></i></a>
                                                            <a href="#"><i className="glyphicon glyphicon-edit"></i></a><a className="del" href="#"><i className="glyphicon glyphicon-trash"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-3 col-sm-4 text-center">
                                                <div className="user-ext-box contact-box">
                                                    <div className="user-ext-details">
                                                        <h2>Jhone Smith</h2>
                                                        <p>demo@gmail.com</p>
                                                        <p>(+91)9418394421</p>
                                                        <div className="row icon-div">
                                                            <a className="call-icon" href="#"><i className="icon-phone icons icons"></i></a>
                                                            <a href="#"><i className="glyphicon glyphicon-edit"></i></a><a className="del" href="#"><i className="glyphicon glyphicon-trash"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-3 col-sm-4 text-center">
                                                <div className="user-ext-box contact-box">
                                                    <div className="user-ext-details">
                                                        <h2>Jhone Smith</h2>
                                                        <p>demo@gmail.com</p>
                                                        <p>(+91)9418394421</p>
                                                        <div className="row icon-div">
                                                            <a className="call-icon" href="#"><i className="icon-phone icons icons"></i></a>
                                                            <a href="#"><i className="glyphicon glyphicon-edit"></i></a><a className="del" href="#"><i className="glyphicon glyphicon-trash"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-3 col-sm-4 text-center">
                                                <div className="user-ext-box contact-box">
                                                    <div className="user-ext-details">
                                                        <h2>Jhone Smith</h2>
                                                        <p>demo@gmail.com</p>
                                                        <p>(+91)9418394421</p>
                                                        <div className="row icon-div">
                                                            <a className="call-icon" href="#"><i className="icon-phone icons icons"></i></a>
                                                            <a href="#"><i className="glyphicon glyphicon-edit"></i></a><a className="del" href="#"><i className="glyphicon glyphicon-trash"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-3 col-sm-4 text-center">
                                                <div className="user-ext-box contact-box">
                                                    <div className="user-ext-details">
                                                        <h2>Jhone Smith</h2>
                                                        <p>demo@gmail.com</p>
                                                        <p>(+91)9418394421</p>
                                                        <div className="row icon-div">
                                                            <a className="call-icon" href="#"><i className="icon-phone icons icons"></i></a>
                                                            <a href="#"><i className="glyphicon glyphicon-edit"></i></a><a className="del" href="#"><i className="glyphicon glyphicon-trash"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-3 col-sm-4 text-center">
                                                <div className="user-ext-box contact-box">
                                                    <div className="user-ext-details">
                                                        <h2>Jhone Smith</h2>
                                                        <p>demo@gmail.com</p>
                                                        <p>(+91)9418394421</p>
                                                        <div className="row icon-div">
                                                            <a className="call-icon" href="#"><i className="icon-phone icons icons"></i></a>
                                                            <a href="#"><i className="glyphicon glyphicon-edit"></i></a><a className="del" href="#"><i className="glyphicon glyphicon-trash"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-3 col-sm-4 text-center">
                                                <div className="user-ext-box contact-box">
                                                    <div className="user-ext-details">
                                                        <h2>Jhone Smith</h2>
                                                        <p>demo@gmail.com</p>
                                                        <p>(+91)9418394421</p>
                                                        <div className="row icon-div">
                                                            <a className="call-icon" href="#"><i className="icon-phone icons icons"></i></a>
                                                            <a href="#"><i className="glyphicon glyphicon-edit"></i></a><a className="del" href="#"><i className="glyphicon glyphicon-trash"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-3 col-sm-4 text-center">
                                                <div className="user-ext-box contact-box">
                                                    <div className="user-ext-details">
                                                        <h2>Jhone Smith</h2>
                                                        <p>demo@gmail.com</p>
                                                        <p>(+91)9418394421</p>
                                                        <div className="row icon-div">
                                                            <a className="call-icon" href="#"><i className="icon-phone icons icons"></i></a>
                                                            <a href="#"><i className="glyphicon glyphicon-edit"></i></a><a className="del" href="#"><i className="glyphicon glyphicon-trash"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-3 col-sm-4 text-center">
                                                <div className="user-ext-box contact-box">
                                                    <div className="user-ext-details">
                                                        <h2>Jhone Smith</h2>
                                                        <p>demo@gmail.com</p>
                                                        <p>(+91)9418394421</p>
                                                        <div className="row icon-div">
                                                            <a className="call-icon" href="#"><i className="icon-phone icons icons"></i></a>
                                                            <a href="#"><i className="glyphicon glyphicon-edit"></i></a><a className="del" href="#"><i className="glyphicon glyphicon-trash"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>



                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>


        )
    }
}
