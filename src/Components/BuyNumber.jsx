import React, { Component } from 'react';

export default class BuyNumber extends Component {
  render() {
    return (
    
         <div className="main_container">
    <div className="page_content">
      <section className="upper_section">
        <h4>Buy Number</h4>
        <div className="pull-right">
			<a href="buy_number_inner.html" className="btn btn-red">Buy Number</a>
        </div>
      </section>
      <section className="lower_section">
        <div className="report-tab"> 
          
         
          <div className="tab-content">
            <div role="tabpanel" className="tab-pane active" id="Payment">
              <div className="container-fluid no-padding">
                <div className="col-md-5 col-sm-4 col-xs-12 m-t-15"><b><div className="summary">Showing <b>1-63</b> of <b>63</b> items.</div> </b></div>
                <form id="perPageRecord-form" data-pjax="" action="">
                  <div className="col-md-2 col-sm-4 col-xs-12 input-sm form-inline m-t-20">
                    <label>Record per page&nbsp;&nbsp;</label>
                    <select  className="form-control record-box no-padding">
                      <option value="20">20</option>
                      <option value="50">50</option>
                      <option value="100">100</option>
                      <option value="50000000">All</option>
                    </select>
                  </div>
                </form>
                <div className="col-md-5 col-sm-4 col-xs-12 no-padding">
                  <ul className="pagination">
                    <li className="prev disabled"><span>«</span></li>
                    <li className="active"><a href="#" data-page="0">1</a></li>
                    <li><a href="#" data-page="1">2</a></li>
                    <li><a href="#" data-page="2">3</a></li>
                    <li><a href="#" data-page="3">4</a></li>
                    <li className="next"><a href="#" data-page="1">»</a></li>
                  </ul>
                </div>
              </div>
              <div className="table-responsive custom_table">
                <table id="example23" className="table table-striped">
                  <thead>
                    <tr>
                      <th># &nbsp;&nbsp;<a href="#"><i className="fa fa-sort"></i></a></th>
                      <th>Number</th>
                      <th>Country&nbsp;&nbsp;<a href="#"><i className="fa fa-sort"></i></a></th>
                      <th>Area &nbsp;&nbsp;<a href="#"><i className="fa fa-sort"></i></a></th>
                      <th>Setup Cost &nbsp;&nbsp;<a href="#"><i className="fa fa-sort"></i></a></th>
                      <th>Monthly Cost &nbsp;&nbsp;<a href="#"><i className="fa fa-sort"></i></a></th>
						 <th>Next Billing &nbsp;&nbsp;<a href="#"><i className="fa fa-sort"></i></a></th>
						<th>Action &nbsp;&nbsp;<a href="#"><i className="fa fa-sort"></i></a></th>
                    </tr>
                  </thead>
                  <tbody>

                    <tr>
                      <td>1</td>
                      <td>123456789</td>
                        <td>India</td>
                      <td>Mohali</td>
                      <td>$2.70</td>
                      <td>$28.70</td>
						 <td>26-02-2018</td>
						<td style="text-align:center;">
						  <a href="#" title="Release Number"><i className="fa fa-sort-numeric-desc"></i></a></td>
                    </tr>
					   <tr>
                      <td>2</td>
                      <td>123456789</td>
                        <td>India</td>
                      <td>Mohali</td>
                      <td>$2.70</td>
                      <td>$28.70</td>
						 <td>26-02-2018</td>
						<td style="text-align:center;">
						  <a href="#" title="Release Number"><i className="fa fa-sort-numeric-desc"></i></a></td>
                    </tr>
					   <tr>
                      <td>3</td>
                      <td>123456789</td>
                        <td>India</td>
                      <td>Mohali</td>
                      <td>$2.70</td>
                      <td>$28.70</td>
						 <td>26-02-2018</td>
						<td style="text-align:center;">
						  <a href="#" title="Release Number"><i className="fa fa-sort-numeric-desc"></i></a></td>
                    </tr>
					   <tr>
                      <td>4</td>
                      <td>123456789</td>
                        <td>India</td>
                      <td>Mohali</td>
                      <td>$2.70</td>
                      <td>$28.70</td>
						 <td>26-02-2018</td>
						<td style="text-align:center;">
						  <a href="#" title="Release Number"><i className="fa fa-sort-numeric-desc"></i></a></td>
                    </tr>
					   <tr>
                      <td>5</td>
                      <td>123456789</td>
                        <td>India</td>
                      <td>Mohali</td>
                      <td>$2.70</td>
                      <td>$28.70</td>
						 <td>26-02-2018</td>
						<td style="text-align:center;">
						  <a href="#" title="Release Number"><i className="fa fa-sort-numeric-desc"></i></a>
                          </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
			  </div>
          </div>
       
	  
      </section>

    </div>
    </div>
   
    )
  }
}
