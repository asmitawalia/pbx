import React, { Component } from 'react';
import './App.css';
import {BrowserRouter as Router, Link , NavLink, Switch} from 'react-router-dom';
import Route  from 'react-router-dom/Route';
import {Abcfunctioncomponent} from './Components/Abcfunctioncomponent';
import Abcc  from './Components/Abcclasscomponent';
import Createelementexample from './Components/Createelementexample';
const Username = ({ match }) => {
  return( <h1> heyyy welcome {match.params.usernamee}</h1>)
}
class App extends Component {
  
  render() {
    return (
      
      <Router>
          <div className="App">
          <Createelementexample/> 
          <Abcfunctioncomponent/>
           <Abcc/>
          <ul>
          <li><NavLink to ="/" style ={
            {
              color:"green"
            }
          } >home page</NavLink></li>
          <li><NavLink to ="/listing">listing</NavLink></li>
          <li><NavLink to ="/profile" >profile</NavLink></li>
          <li><NavLink to ="/user/asmita" >user(asmita)</NavLink></li>
          <li><NavLink to ="/user/walia" >user(walia)</NavLink></li>
          <li><NavLink to ="/user/shaina" >user(shaina)</NavLink></li>
          </ul>
          <Switch>
          {/* //<Route path="/listing" strict exact render component ={Createelementexample}/> */}
          <Route exact path="/listing" component={Abcc}/>
            </Switch>
                                               {/* this method is simple for using render method a */}
              <Route path="/" strict exact render ={
                ()=>{
                  return(
                    <h1>heyyy this is our first page </h1>
                  )
                }
              }
              />
              <Route path="/profile" strict exact render ={
                ()=>{
                  return(
                    <h1>heyyy this is profile page </h1>
                  )
                }
              }
              />
                                              {/* this is using components which we have defined earlier */}
              <Route path="/user/:usernamee" strict exact component ={Username}/>
              
          </div>
      </Router>
     
    );
  }
}

export default App;
