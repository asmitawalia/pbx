import React, { Component } from "react";
import {
  BrowserRouter as Router,
  NavLink,
  Switch
} from "react-router-dom";
import Route from "react-router-dom/Route";
import Dashboard from "./Dashboard";
import BuyNumber from "./BuyNumber";
import Contacts from "./Contacts";
import SmsMms from "./SmsMms";
export default class SideMenu extends Component {
  render() {
    return (
      <Router>
        <div>
          <div id="page_container">
            <aside id="aside">
              <ul className="nav">
                <li>
                  <NavLink to="/Dashboard">
                    <i className="icon-home icons" />
                    <span>Dashboard</span>
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/BuyNumber">
                    <i className="icon-equalizer icons" />
                    <span>Buy Number</span>
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/Dashboard">
                    <i className="icon-people icons" />
                    <span>Teams/User</span>
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/BuyNumber">
                    <i className="icon-phone icons" />
                    <span>Phone System</span>
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/Contacts">
                    <i className="icon-notebook icons" />
                    <span>Contacts</span>
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/SmsMms">
                    <i className="icon-notebook icons" />
                    <span>SMS/MMS</span>
                  </NavLink>
                </li>
              </ul>
            </aside>
          </div>
          <div className="main_container">
            <div className="page_content">
              <Switch>
                <Route path="/Dashboard" strict exact component={Dashboard} />
                <Route path="/BuyNumber" strict exact component={BuyNumber} />
                <Route path="/Dashboard" strict exact component={Dashboard} />
                <Route path="/BuyNumber" strict exact component={BuyNumber} />
                <Route path="/Contacts" strict exact component={Contacts} />
                <Route path="/SmsMms" strict exact component={SmsMms} />
              </Switch>
            </div>
          </div>
        </div>
      </Router>
    );
  }
}
