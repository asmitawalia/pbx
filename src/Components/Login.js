import React, { Component } from 'react'

export default class Login extends Component {
        constructor(props)
        {
            super(props)
            let loggedIn = false
            this.state = {
            email: '',
            password: '',
            loggedIn
           }
           this.onChange   = this.onChange.bind(this)
           this.submitForm = this.submitForm.bind(this)
        }
        onChange(e)
        {
          this.setState({
            [e.target.name]: e.target.value
          })
        }
        submitForm(e)
        {
          e.preventDefault()
          {
            const { email,password } = this.state
            //login Magic
          }
        }
  render() 
  {
    return (
      <div className="login-body-wrapper login-body-login">
        	<div className="login-body-content">
          <div className="col-md-4 login-left">
			  	<div className="lg-content">
				   	<h2>V3M</h2>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
					<a href="111" className="btn btn-red">Support</a>
			  	</div>
		    	</div>
          <div className="login-main-content col-md-8 pull-right">
          <div className="login-container">
          <div className="panel panel-default">
          <div className="panel-heading"><img src="assets/img/logo_inner.png" alt="logo" className="logo-img" /> <span className="login-description">Please login with your user information.</span>
          </div>
          <div className="panel-body">
          <form onSubmit = {this.submitForm}>
          <div className="form-group">
								<input type="email" placeholder="Enter Your Email" className="form-control" name="email" value={this.state.email} onChange={this.onChange} />
					</div>
          <div className="form-group">
								<input type="password" placeholder="******" className="form-control" name="password" value={this.state.password} onChange={this.onChange} />
					</div>
          <div className="form-group row login-tools">
								<div className="col-xs-6 login-remember">
									<div className="log-body-checkbox">
										<input type="checkbox" id="remember" />
										<label htmlFor="remember">Remember me</label>
									</div>
								</div>
								<div className="col-xs-6 login-forgot-password"><a href="1111">Forgot Password?</a>
								</div>
							</div>
              <div className="form-group login-submit">
								<button type="submit" className="btn btn-red btn-block">Login</button>
							</div>
							<label className="text-danger"></label>	
          </form>
          </div>
          </div>
          </div>
          </div>
          </div>
      </div>
    )
  }
}
