import React, { Component } from 'react'

export default class Header extends Component {
  render() {
    return (
      <div>
        <header id="header">

          <div className="navbar-header">
            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span className="sr-only">Toggle navigation</span> <span className="icon-bar"></span> <span className="icon-bar"></span> <span className="icon-bar"></span> </button>
            <a href="#" className="navbar-brand" /><img src="assets/img/logo_inner.png"/>

            <a data-toggle="collapse" href="#aside" className="visible-xs pull-left" aria-expanded="false" aria-controls="collapseExample" /><i className="fa fa-indent" />
          </div>

          <nav className="navbar navbar-default">
            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

              <ul className="nav navbar-nav navbar-right">
                <li><a href="#" className="dialer-icon" data-toggle="modal" data-target="#call-dailer"><i className="icon-phone icons"></i></a></li>
                <li><a href="transactions.html"><i className="icon-disc icons"></i><span>Transactions</span></a></li>
                <li><a href="reports.html"><i className="icon-docs icons"></i><span>Reports</span></a></li>
                <li><a href="support.html"><i className="icon-support icons"></i><span>Support</span></a></li>
                <li className="dropdown profile-drop"> <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="assets/img/profile_icon_2.png" alt="profile" /></a>
                  <ul className="dropdown-menu">
                    <li><a href="#">My Profile</a></li>
                    <li><a href="#">Plans</a></li>
                    <li><a href="#">Setting</a></li>
                    <li><a href="#">Logout</a></li>
                  </ul>
                </li>

              </ul>
            </div>
          </nav>
        </header></div>
    )
  }
}
